package my.home.z032.benchmark;

import static my.home.z032.util.TestHelper.printStatsForWarmUp;
import static my.home.z032.util.TestHelper.xorifyNewvalueWithOldValue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import my.home.z032.service.RngService;
import my.home.z032.util.ResultsToCsv;
import my.home.z032.util.TestResults;

/**
 * Run single micro benchmark test for given service implementation on given
 * number of threads.
 * <p>
 * Implementation notes:
 * <li>warm up period - run unmeasured enough times to exclude any runtime
 * compilation compromising results
 * <li>use dummy variable to prevent optimizer to ignore large sections of
 * measured code
 * <li>it should run for minutes
 * <li>for GC is not triggered during measurement compromising results
 * preallocate as much memory as possible
 */
public class RngConcurrentPerformanceTest {

	private final int numberOfThreads;
	public static final int TRIALS_PER_THREAD = 100_000;

	public static final int NUMBER_OF_WARM_UP_ATTEMPTS = 50_000;

	private final CountDownLatch startLatch;
	private final CountDownLatch finishLatch;

	private RngService service;

	public static final int RANDOM_BYTES_LENGTH = 16;

	private byte[] dummy = new byte[RANDOM_BYTES_LENGTH];

	private ResultsToCsv resultsToCsv = new ResultsToCsv();

	public RngConcurrentPerformanceTest(RngService service, int numberOfThreads) {
		this.service = service;
		this.numberOfThreads = numberOfThreads;
		startLatch = new CountDownLatch(1);
		finishLatch = new CountDownLatch(numberOfThreads);
	}

	/**
	 * Run benchmark
	 * 
	 * @return gathered measurements
	 */
	public TestResults run() {
		System.out.println("Implementation " + service.getClass()
				+ " threads: " + numberOfThreads);

		// setup threads
		ExecutorService ec = Executors.newCachedThreadPool();
		//ExecutorService ec = Executors.newFixedThreadPool(8);
		for (int i = 0; i < numberOfThreads; i++) {
			ec.execute(new Runnable() {
				@Override
				public void run() {
					byte[] dummy = new byte[16];
					try {
						startLatch.await();
						for (int i = 0; i < TRIALS_PER_THREAD; i++) {
							byte[] dummy0 = service.getRandomBytes();
							dummy = xorifyNewvalueWithOldValue(dummy, dummy0);
							// if (i%100==0) Thread.yield();
						}
						finishLatch.countDown();
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});
		}

		// warm up
		System.out.println("Warm-up round start");
		for (int i = 0; i < NUMBER_OF_WARM_UP_ATTEMPTS; i++) {
			byte[] dummy0 = service.getRandomBytes();
			dummy = xorifyNewvalueWithOldValue(dummy, dummy0);
		}
		printStatsForWarmUp(1, NUMBER_OF_WARM_UP_ATTEMPTS, service);
		System.out.println("Free memory: " + Runtime.getRuntime().freeMemory());

		// do the real test, start measuring
		try {
			// start measuring
			System.out.println("Let's do it for real now, start measuring");
			long t1 = System.nanoTime();
			// start workers all at once
			startLatch.countDown();

			// wait until last worker finishes
			finishLatch.await();
			long t2 = System.nanoTime();
			System.out.println("testing finished");
			TestResults r = new TestResults(t1, t2, numberOfThreads,
					TRIALS_PER_THREAD, service.getClass());
			r.printStats();
			resultsToCsv.writeResult(r);
			return r;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			ec.shutdown();
		}
	}
}
