package my.home.z032.benchmark;

import static my.home.z032.util.TestHelper.printStatsForWarmUp;
import static my.home.z032.util.TestHelper.xorifyNewvalueWithOldValue;
import my.home.z032.service.RngService;
import my.home.z032.util.TestResults;

/**
 * Run single micro benchmark test for given service implementation on one thread.
 * <p>
 * Implementation notes:
 * <li>warm up period - run unmeasured enough times to exclude any runtime
 * compilation compromising results
 * <li>use dummy variable to prevent optimizer to ignore large sections of
 * measured code
 * <li>it should run for minutes
 * <li>for GC is not triggered during measurement compromising results
 * preallocate as much memory as possible
 */
public class RngSimplePerformanceTest {

	public static final int TRIALS_PER_THREAD = 10_000_000;

	public static final int NUMBER_OF_WARM_UP_ATTEMPTS = 50_000;
	
	private RngService service;
	
	public static final int RANDOM_BYTES_LENGTH = 16;
	
	private byte[] dummy = new byte[RANDOM_BYTES_LENGTH];
	
	public RngSimplePerformanceTest(RngService service) {
		this.service = service;
	}
	
	public void run() {
		System.out.println("Implementation " + service.getClass() + " threads: 1");
		
		// warm up
		for (int i = 0; i < NUMBER_OF_WARM_UP_ATTEMPTS; i++) {
			byte[] dummy0 = service.getRandomBytes();
			dummy = xorifyNewvalueWithOldValue(dummy, dummy0);
		}
		printStatsForWarmUp(1, NUMBER_OF_WARM_UP_ATTEMPTS, service);
		
		// do the real test, start measuring
		long t1 = System.nanoTime();
		for (int i = 0; i < NUMBER_OF_WARM_UP_ATTEMPTS; i++) {
			byte[] dummy0 = service.getRandomBytes();
			dummy = xorifyNewvalueWithOldValue(dummy, dummy0);
		}
		long t2 = System.nanoTime();
		TestResults r = new TestResults(t1, t2, 1, TRIALS_PER_THREAD, service.getClass());
		r.printStats();
		
		//System.out.println("testing finished..");
		//double t = (double)(t2 - t1) / (double)TRIALS;
		//double throughput_in_ms = (double)t/(double)1_000_000;
		//System.out.println(String.format("Avg call last %f ms", throughput_in_ms));
	}
}
