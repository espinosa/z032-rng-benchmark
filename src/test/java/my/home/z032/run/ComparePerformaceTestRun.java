package my.home.z032.run;

import my.home.z032.benchmark.RngConcurrentPerformanceTest;
import my.home.z032.service.GeneratorPerRequest;
import my.home.z032.service.SharedGenerator;
import my.home.z032.util.TestResults;

public class ComparePerformaceTestRun {
	public static void main(String[] args) {
		runBoth(1);
		runBoth(2);
		runBoth(4);
		runBoth(8);
		runBoth(16);
		runBoth(50);
		runBoth(100);
	}
	
	public static void runBoth(int numberOfThreads) {
		TestResults r1 = new RngConcurrentPerformanceTest(new GeneratorPerRequest(), numberOfThreads).run();
		TestResults r2 = new RngConcurrentPerformanceTest(new SharedGenerator(), numberOfThreads).run();
		System.out.println(String.format("Comparison %.2f", (double)r1.itetarionsPerSecond/r2.itetarionsPerSecond));
		System.out.println("-----");
	}
}
