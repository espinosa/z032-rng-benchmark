package my.home.z032.run;

import my.home.z032.benchmark.RngConcurrentPerformanceTest;
import my.home.z032.service.SharedGenerator;

public class ComparePerformaceTestShared100Run {
	public static void main(String[] args) {
		new RngConcurrentPerformanceTest(new SharedGenerator(), 100).run();
	}
}
