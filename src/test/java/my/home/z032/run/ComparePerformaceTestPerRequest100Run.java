package my.home.z032.run;

import my.home.z032.benchmark.RngConcurrentPerformanceTest;
import my.home.z032.service.GeneratorPerRequest;

public class ComparePerformaceTestPerRequest100Run {
	public static void main(String[] args) {
		new RngConcurrentPerformanceTest(new GeneratorPerRequest(), 100).run();
	}
}
