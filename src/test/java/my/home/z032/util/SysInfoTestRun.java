package my.home.z032.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SysInfoTestRun {
	public static void main(String[] args) {
		System.out.println("os.name=" + System.getProperty("os.name")); // Linux
		System.out.println("os.arch=" + System.getProperty("os.arch")); // amd64
		// Java's "os.arch" System Property is the Bitness of the JRE, NOT the
		// Operating System
		// http://mark.koli.ch/javas-osarch-system-property-is-the-bitness-of-the-jre-not-the-operating-system

		// Linux kernel version: example 3.11.10-7-desktop
		System.out.println("os.version=" + System.getProperty("os.version"));

		int numberOfCpuCores = Runtime.getRuntime().availableProcessors();
		System.out.println("numberOfCpuCores=" + numberOfCpuCores);

		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostname = "unknown";
		}
		System.out.println("hostname=" + hostname);

		// works only on Windows
		System.out.println("PROCESSOR_IDENTIFIER="
				+ System.getenv("PROCESSOR_IDENTIFIER"));

		// works only on Windows
		System.out.println("PROCESSOR_ARCHITEW6432="
				+ System.getenv("PROCESSOR_ARCHITEW6432"));

		// http://docs.oracle.com/javase/7/docs/api/java/lang/System.html#getProperties%28%29
		System.out.println("java.version=" + System.getProperty("java.version"));
		// Java Runtime Environment version
		System.out.println("java.vendor=" + System.getProperty("java.vendor"));
		// Java Runtime Environment vendor


		System.out.println("java.vm.specification.version="
				+ System.getProperty("java.vm.specification.version"));
		// Java Virtual Machine specification version
		System.out.println("java.vm.specification.vendor="
				+ System.getProperty("java.vm.specification.vendor"));
		// Java Virtual Machine specification vendor
		System.out.println("java.vm.specification.name="
				+ System.getProperty("java.vm.specification.name"));
		// Java Virtual Machine specification name
		
		System.out.println("java.vm.version=" + System.getProperty("java.vm.version"));
		// Java Virtual Machine implementation version
		System.out.println("java.vm.vendor=" + System.getProperty("java.vm.vendor"));
		// Java Virtual Machine implementation vendor
		System.out.println("java.vm.name=" + System.getProperty("java.vm.name"));
		// Java Virtual Machine implementation name
		System.out.println("java.specification.version="
				+ System.getProperty("java.specification.version"));
		// Java Runtime Environment specification version
		System.out.println("java.specification.vendor="
				+ System.getProperty("java.specification.vendor"));
		// Java Runtime Environment specification vendor
		System.out.println("java.specification.name="
				+ System.getProperty("java.specification.name"));
		// Java Runtime Environment specification name
	}
}
