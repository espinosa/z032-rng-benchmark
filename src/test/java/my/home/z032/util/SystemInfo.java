package my.home.z032.util;

/**
 * Encapsulate some basic data about system running the benchmark to be attached
 * to filed results
 */
public class SystemInfo {

	public final String hostname;

	/** operating system ID - Linux, Windows, Mac OS, Solaris .. */
	public final String osName;

	public final int numberOfCpuCores;

	/** Java Runtime Environment version */
	public final String javaVersion;

	/** Java Runtime Environment vendor */
	public final String javaVendor;
	
	/** java.vm.name, Example: "Java HotSpot(TM) 64-Bit Server VM" */
	public final String vmName; 

	public SystemInfo(String hostname, String osName, int numberOfCpuCores,
			String javaVersion, String javaVendor, String vmName) {
		this.hostname = hostname;
		this.osName = osName;
		this.numberOfCpuCores = numberOfCpuCores;
		this.javaVersion = javaVersion;
		this.javaVendor = javaVendor;
		this.vmName = vmName;
	}

	@Override
	public String toString() {
		return "hostname=" + hostname + ", osName=" + osName + ", jvm="
				+ vmName + " " + javaVersion + " " + javaVendor + ", numberOfCpuCores="
				+ numberOfCpuCores;
	}
}
