package my.home.z032.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Write {@link TestResults} to CSV file located inside projects
 */
public class ResultsToCsv {
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat(
			"YYYY-MM-dd hh:mm:ss");

	public SystemInfoProvider systemInfoProvider = new SystemInfoProvider();

	public void writeResult(TestResults r) {
		String csvFileName = "results.csv";
		File csvFile = new File(csvFileName);
		boolean newFileSoPrintHeader = !csvFile.exists();

		boolean appendToExistingFile = true;
		try (CSVWriter writer = new CSVWriter(new FileWriter(csvFile,
				appendToExistingFile))) {
			
			if (newFileSoPrintHeader) {
				printHeader(writer);
			}

			List<String> entries = new LinkedList<>();

			// implementation class name
			entries.add(r.implClass.getSimpleName());

			// Number of worker threads
			entries.add(Integer.toString(r.numberOfWorkers));

			// Number of trials per worker
			entries.add(Integer.toString(r.trialsPerWorker));

			// Total number of iterations
			entries.add(Integer.toString(r.totalIterationsNo));

			// Elapsed time: %1.2fs (should be > 10s)
			entries.add(Double.toString(r.elapsedInSec));

			// Average time per iteration: %1.5fms",
			entries.add(Double.toString(r.avgIterationTimeInMs));

			// Throughput: %,d transactions per second",
			entries.add(Double.toString(r.itetarionsPerSecond));
			
			// date
			entries.add(dateFormatter.format(new Date()));

			// system and OS info
			entries.add(systemInfoProvider.get().toString());
			
			writer.writeNext(entries.toArray(new String[0]));

		} catch (IOException e) {
			throw new RuntimeException("Error when opening or writing to file "
					+ csvFileName, e);
		}
	}

	protected void printHeader(CSVWriter writer) {
		List<String> entries = new LinkedList<>();
		entries.add("Implementation class");
		entries.add("Number of threads");
		entries.add("Trials per worker");
		entries.add("Total number of iterations");
		entries.add("Total Elapsed time in sec");
		entries.add("Average time per iteration in ms");
		entries.add("Throughput in transactions per second");
		entries.add("Run date");
		entries.add("System info");
		writer.writeNext(entries.toArray(new String[0]));
	}
}
