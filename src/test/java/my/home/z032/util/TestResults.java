package my.home.z032.util;


public class TestResults {

	public final long startTimeInNano;
	public final long stopTimeInNano;
	public final int numberOfWorkers;
	public final int trialsPerWorker;
	public final Class<?> implClass;

	public final int totalIterationsNo;
	public final double elapsedInNano;
	public final double elapsedInMs;
	public final double elapsedInSec;
	public final int itetarionsPerSecond;
	public final double avgIterationTimeInMs;

	public TestResults(long startTimeInNano, long stopTimeInNano,
			int numberOfWorkers, int trialsPerWorker, Class<?> implClass) {
		this.startTimeInNano = startTimeInNano;
		this.stopTimeInNano = stopTimeInNano;
		this.numberOfWorkers = numberOfWorkers;
		this.trialsPerWorker = trialsPerWorker;
		this.implClass = implClass;

		totalIterationsNo = numberOfWorkers * trialsPerWorker;
		elapsedInNano = stopTimeInNano - startTimeInNano;
		elapsedInMs = (double) elapsedInNano / 1000000;
		elapsedInSec = (double) elapsedInNano / 1000000000;
		itetarionsPerSecond = (int) (totalIterationsNo / elapsedInSec);
		avgIterationTimeInMs = (double) elapsedInMs / totalIterationsNo;
	}

	public void printStats() {
		System.out.println(String.format("Implementation %s", implClass.getName()));
		System.out.println(String.format("Number of worker threads %d",
				numberOfWorkers));
		System.out.println(String.format("Number of trials per worker %,d",
				trialsPerWorker));
		System.out.println(String.format("Total number of iterations %,d",
				totalIterationsNo));
		System.out.println(String.format(
				"Elapsed time: %1.2fs (should be > 10s)", elapsedInSec));
		System.out.println(String.format("Average time per iteration: %1.5fms",
				avgIterationTimeInMs));
		System.out
				.println(String.format(
						"Throughput: %,d transactions per second",
						itetarionsPerSecond));
	}
}
