package my.home.z032.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Gather data about system running the benchmark to be attached to filed
 * results
 */
public class SystemInfoProvider {

	public SystemInfo get() {
		String osName = System.getProperty("os.name");
		// String osType= System.getProperty("os.arch");
		// String osVersion= System.getProperty("os.version");

		int numberOfCpuCores = Runtime.getRuntime().availableProcessors();

		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostname = "unknown";
		}

		String javaVersion = System.getProperty("java.version"); // Java Runtime Environment version
		
		String javaVendor=System.getProperty("java.vendor"); // Java Runtime Environment vendor
		
		String vmName=System.getProperty("java.vm.name"); // Java Runtime Environment vendor
		
		return new SystemInfo(hostname, osName, numberOfCpuCores, javaVersion, javaVendor, vmName);
	}
}
