package my.home.z032.util;

import my.home.z032.service.RngService;

public class TestHelper {

	/**
	 * XORify two arrays of the same length the quickest possible way, for
	 * testing purposes.
	 */
	public static byte[] xorifyNewvalueWithOldValue(byte[] a1, byte[] a2) {
		for (int i = 0; i < a1.length; i++) {
			a1[i] = (byte) (a1[i] + a2[i]);
		}
		return a1;
	}

	public static void printStatsForWarmUp(int numberOfWorkers,
			int trialsPerWorker, RngService generatorService) {
		int totalIterationsNo = numberOfWorkers * trialsPerWorker;
		System.out.println(String.format(
				"warm-up: Number of worker threads %d", numberOfWorkers));
		System.out.println(String.format(
				"warm-up: Number of trials per worker %,d", trialsPerWorker));
		System.out.println(String.format(
				"warm-up: Total number of iterations %,d", totalIterationsNo));
	}
}
