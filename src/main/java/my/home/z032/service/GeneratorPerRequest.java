package my.home.z032.service;

import java.security.SecureRandom;

/**
 * Service where RNG is created for every request
 */
public class GeneratorPerRequest implements RngService {
	
	@Override
	public byte[] getRandomBytes() {
		//  val bytes = Array.ofDim[Byte](16)
	    //  val rnd = new SecureRandom()
	    //  rnd.setSeed(DateTime.now.getMillis())
	    //  rnd.nextBytes(bytes)
	    //  bytes
		byte[] r = new byte[16];
		SecureRandom rng = new SecureRandom();
		rng.setSeed(System.currentTimeMillis());
		rng.nextBytes(r);
		return r;
	}
}
