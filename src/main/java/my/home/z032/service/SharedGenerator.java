package my.home.z032.service;

import java.security.SecureRandom;

public class SharedGenerator implements RngService {
	// define RNG as singleton 
	private static SecureRandom rng = new SecureRandom();
	static {
		// initialize RNG seed
		rng.setSeed(System.currentTimeMillis());
		//rng.nextBytes(new byte[16]);
	}

	@Override
	public byte[] getRandomBytes() {
		byte[] r = new byte[16];
		rng.nextBytes(r);
		return r;
	}
}
