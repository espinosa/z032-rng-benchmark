Microbenchmark for comparison of different implementations of secure Random Number Generator handling in concurrent environment. There are two competing implementations:

   * **shared instance** of a random generator, a singleton, classic recommended approach 

   * **per request** - random generator is fully instantiated with every request.
     Benefit: no state, functional friendly approach  
     Disadvantage: constructing SecureRandom is expensive operation  

Documentation
-------------

Run as:

    mvn test
    
_This launches benchmarks, one by one, in separate JVMs, solution based on Maven and inline Groovy_

Results are stored in `results.csv` under the project root directory. It is dynamically constructed 
on the first run. 

Test runs for several minutes on slower computers! On mine it is currently
ca 0.5 minute one.

Currently both benchmarks run **100 concurrent threads** to create **maximum contention**, so in fact they are biased towards _per request_ variant.

Every implementation under fresh forked JVM. 
Every test runs with system default `java` with attributes `-XX:+PrintCompilation` and `-verbose:gc`.
Some basic runtime data are stored with every test, data like number of CPU cores, OS, 
JVM version and vendor name.

Lots of interesting details are also printed on the console.
What to watch out for:

  * It should not indicate any run time compilation after warm up period.
    That is why option `-XX:+PrintCompilation` is used.
  * It should not indicate active GC (garbage collector) after warm-up period, or better any time, as 
    this test does not create many objects.  
    That is why `-verbose:gc` is used.


Technical Notes
---------------
   
Indication of active GC:  
_ideally, there should be any or at least minimum_

    [GC 32256K->560K(121344K), 0.0013430 secs]                                                                                                                              
    [GC 32816K->536K(121344K), 0.0020320 secs]                                                                                                                              
    [GC 32792K->560K(121344K), 0.0010730 secs]
   
Indication of JVM run time compilation:  
_should not happen during the live measurement, or at least not for measured classes. All such runtime compilation should run only in warm up phase. That is
the warm up phase is there._

     38    1             java.util.Properties$LineReader::readLine (452 bytes)
     41    2             java.lang.String::hashCode (55 bytes)
     51    3             sun.security.provider.SHA::implCompress (491 bytes)
     60    4             java.lang.Integer::reverseBytes (26 bytes)


Alternative launchers
---------------------

    mvn -f pom.withExec.xml exec:exec
    
This launches benchmark, in separate JVMs, solution based on Maven and maven-execution-plugin. Unfortunately, only one test can be run this way.
 
    mvn -f pom.withAntRun.pom test
    
This launches benchmark, in separate JVMs, solution based on Maven and maven-antrun-plugin. This variant runs all test but it gives
negligibly worse times, for both variants, negligibly but still. So I created the third and now default variant based on inline Groovy script:

    mvn test   
 
Alternative run without Maven

    java -Xms2g -Xmx2g -XX:+PrintCompilation -verbose:gc -classpath ./target/test-classes:./target/classes my.home.z032.run.ComparePerformaceTestShared100Run
    java -Xms2g -Xmx2g -XX:+PrintCompilation -verbose:gc -classpath ./target/test-classes:./target/classes my.home.z032.run.ComparePerformaceTestPerRequest100Run
 
_note: the 100 means 100 threads._
    
